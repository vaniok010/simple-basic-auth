<?php

return [
    'credentials' => env('SIMPLE_BASIC_AUTH_CREDENTIALS', ''), // example: "login1=pass1;login2=pass2"
    'credentialDelimiter' => ';',
    'loginPasswordDelimiter' => '=',
];
