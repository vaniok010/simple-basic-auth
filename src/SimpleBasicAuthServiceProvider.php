<?php

namespace Hryha\SimpleBasicAuth;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class SimpleBasicAuthServiceProvider extends ServiceProvider
{
    /**
     * @throws BindingResolutionException
     */
    public function boot(): void
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/simple-basic-auth.php',
            'simple-basic-auth'
        );

        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('simple-basic-auth', SimpleBasicAuthMiddleware::class);
    }

    public function register(): void
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/simple-basic-auth.php' => config_path('simple-basic-auth.php'),
            ], 'config');
        }
    }
}
