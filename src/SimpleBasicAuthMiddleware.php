<?php

namespace Hryha\SimpleBasicAuth;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;

class SimpleBasicAuthMiddleware
{
    private array $users = [];

    /**
     * @return mixed
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next, ...$logins)
    {
        $this->getUsers($logins);

        $user = $request->server->get('PHP_AUTH_USER', '');
        $password = $request->server->get('PHP_AUTH_PW', '');

        throw_if(
            false === $this->checkUserPassword($user, $password),
            new UnauthorizedHttpException('Basic')
        );

        return $next($request);
    }

    /**
     * @throws Throwable
     */
    private function getUsers(array $onlyLogins = []): void
    {
        throw_if(
            empty(config('simple-basic-auth.credentials')),
            'You need set up credentials for simple basic auth in your env file'
        );

        $credentials = explode(config('simple-basic-auth.credentialDelimiter'), config('simple-basic-auth.credentials'));

        foreach ($credentials as $credential) {
            if (false === mb_strpos($credential, config('simple-basic-auth.loginPasswordDelimiter'))) {
                continue;
            }

            list($login, $password) = array_map('trim', explode(config('simple-basic-auth.loginPasswordDelimiter'), $credential));

            if (! empty($onlyLogins) && ! in_array($login, $onlyLogins)) {
                continue;
            }

            if (! empty($login) && ! empty($password)) {
                $this->users[$login] = $password;
            }
        }
    }

    private function checkUserPassword(string $login, string $password): bool
    {
        if (empty($login) || empty($password)) {
            return false;
        }

        if (! isset($this->users[$login])) {
            return false;
        }

        return 0 === strcmp($password, $this->users[$login]);
    }
}
