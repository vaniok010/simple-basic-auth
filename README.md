# Laravel Simple Basic Auth


This package allows you to add a HTTP Basic Auth filter on your routes, without the need to use a database – which the Laravel default `simple-basic-auth`- middleware relies on.

On failed authentication the user will get a "401 Unauthorized" response.

## Installation

Via Composer

``` bash
$ composer require hryha/simple-basic-auth
```

This package uses Package Auto-Discovery for loading the service provider

If you would like to manually add the provider, turn off Auto-Discovery for the package in your composer.json-file:

``` json
"extra": {
    "laravel": {
        "dont-discover": [
            "hryha/simple-basic-auth"
        ]
    }
},
```

And then add the provider in the providers array (`config/app.php`).

``` php
'providers' => [
    Hryha\SimpleBasicAuth\SimpleBasicAuthServiceProvider::class
]
```

## Configuration

Run the command `$ php artisan vendor:publish --provider="Hryha\SimpleBasicAuth\SimpleBasicAuthServiceProvider"` to directly publish the files.

The file `simple-basic-auth.php` will then be copied to your `app/config` folder – here you can set various logins and passwords.

## Usage

The middleware uses the `simple-basic-auth` filter to protect routes. You can either use `Route::group()` to protect multiple routes, or chose just to protect them individually.

**Group**
``` php
Route::group(['middleware' => 'simple-basic-auth'], function() {
    ...
});
```

**Single**
``` php
Route::get('/', ['middleware' => 'simple-basic-auth']);
```

You may also set the user logins inline;

``` php
Route::get('/', ['middleware' => 'simple-basic-auth:login1,login2']);
```

## License

The MIT License (MIT).
